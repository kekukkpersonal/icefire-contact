# Icefire Contact

### What is this repository for? ###

This is Icefire front-end test exercise.

### How to use? ###

main class location: /src/main/java/ee/kertkukk/IcefireContactApplication.java

start back-end server (http://localhost:8080/):
* run IcefireContactApplication.java

static resources location: /src/main/resources/static

Install packages:
* npm install

Run Angular dev server (http://localhost:3001/):
* npm run serve

Build front-end application:
* npm run build

### Possible errors if using IDEA ###

* Error ->
![alt text](https://i.imgur.com/H2ubCqA.png)
* Reason: We are using Lombok and annotation processing isn't enabled.
* Solution: To resolve this you need to enable annotation processing ->
![alt text](https://i.imgur.com/7r4jQZ3.png)
