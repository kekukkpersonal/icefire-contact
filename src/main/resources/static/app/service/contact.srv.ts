import {Http, Response} from "@angular/http";
import {Injectable} from "@angular/core";
import {Address} from "./address.srv";

export class Contact {

    constructor(public fullName: string,
                public phone: string,
                public mail: string,
                public addresses: Address[],
                public awareOfTerms: boolean,
                public id?: number) {
    };

}

@Injectable()
export class ContactService {
    constructor(private http: Http) {
    }

    getContacts(): Promise<Contact[]> {
        return this.http
            .get('api/contacts')
            .toPromise()
            .then((response: Response) => response.json());
    }

    getContact(id: number): Promise<Contact> {
        return this.http
            .get('api/contact/' + id)
            .toPromise()
            .then((response: Response) => response.json());
    }

    saveContact(contact: Contact): Promise<void> {
        return this.http
            .post('api/contacts', contact)
            .toPromise()
            .then(() => <void>null);
    }

    saveContactAndReturn(contact: Contact): Promise<Contact> {
        return this.http
            .post('api/contact', contact)
            .toPromise()
            .then((response: Response) => response.json());
    }

    deleteContact(id: number): Promise<void> {
        return this.http
            .delete('api/contact/' + id)
            .toPromise()
            .then(() => <void>null);
    }

}