import {Http, Response} from "@angular/http";
import {Injectable} from "@angular/core";

export class Address {

    id: number;
    customer_id: number;

    constructor(public street?: string,
                public village?: string,
                public city?: string,
                public receiver?: boolean) {
    };

}

@Injectable()
export class AddressService {
    constructor(private http: Http) {
    }

    getAddresses(): Promise<Address[]> {
        return this.http
            .get('api/addresses')
            .toPromise()
            .then((response: Response) => response.json());
    }

    getAddress(id: number): Promise<Address> {
        return this.http
            .get('api/address/' + id)
            .toPromise()
            .then((response: Response) => response.json());
    }

    saveAddress(address: Address): Promise<void> {
        return this.http
            .post('api/addresses', address)
            .toPromise()
            .then(() => <void>null);
    }

    deleteAddress(id: number): Promise<void> {
        return this.http
            .delete('api/address/' + id)
            .toPromise()
            .then(() => <void>null);
    }

}