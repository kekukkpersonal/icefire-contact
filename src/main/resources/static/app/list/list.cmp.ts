import {Component} from '@angular/core';
import {Contact, ContactService} from "../service/contact.srv";
import {Router} from "@angular/router";

export enum DeliveryLocation {
    same = "same",
    different = "different"
}

@Component({
    selector: 'list',
    templateUrl: 'app/list/list.html',
    styleUrls: ['app/list/list.css']
})
export class ListComponent {

    contacts: Contact[] = [];
    location = "";

    constructor(private contactService: ContactService, private router: Router) {
        this.location = router.url;
        this.updateContacts();
    }

    updateContacts() {
        this.contactService.getContacts()
            .then(contacts => this.contacts = contacts);
    }

}
