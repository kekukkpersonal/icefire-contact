import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {HttpModule} from '@angular/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {AppComponent}  from './app.cmp';
import {AddressService} from "./service/address.srv";
import {RouterModule} from "@angular/router";
import {routes} from "./routes";
import {ListComponent} from "./list/list.cmp";
import {ViewComponent} from "./form/form.cmp";
import {ContactService} from "./service/contact.srv";
import {NullDefaultValueDirective} from "./directive/nullDefaultValue.directive";
import {HomeComponent} from "./home/home.cmp";

@NgModule({
    imports: [BrowserModule, HttpModule, ReactiveFormsModule, RouterModule.forRoot(routes, {useHash: true}), FormsModule],
    declarations: [AppComponent, HomeComponent, ListComponent, ViewComponent, NullDefaultValueDirective],
    providers: [AddressService, ContactService],
    bootstrap: [AppComponent]
})
export class AppModule {
}