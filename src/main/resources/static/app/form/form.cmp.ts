import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router'

import {Contact, ContactService} from "../service/contact.srv";
import {
    AbstractControl, FormArray, FormBuilder, FormControl, FormGroup, ValidationErrors,
    Validators
} from "@angular/forms";
import {DeliveryLocation} from "../list/list.cmp";
import {Address} from "../service/address.srv";

export enum Routes {
    add = "add",
    view = "view",
    edit = "edit"
}

@Component({
    templateUrl: 'app/form/form.html',
    styleUrls: ['app/form/form.css']
})
export class ViewComponent implements OnInit {

    contact: Contact;
    contactForm: FormGroup;
    deliveryOption: String;
    isDisabled: boolean = false;
    currentRoute: Routes;
    routes = Routes;

    constructor(private route: ActivatedRoute,
                private router: Router,
                private fb: FormBuilder,
                private contactService: ContactService) {
        this.createForm();
    }

    ngOnInit(): void {
        const initialRadioButtonState = DeliveryLocation.same.toString();
        this.deliveryOption = initialRadioButtonState;

        this.chooseRouterLogic();

        //add extra address in case contact chooses to have 2 addresses
        this.addAddress();
    }

    goToChange() {
        this.router.navigateByUrl('edit/' + this.contact.id);
    }

    deleteContact() {
        this.contactService.deleteContact(this.contact.id);
        this.router.navigateByUrl('list');
    }

    addRoute() {
        this.currentRoute = Routes.add;
        this.createContactFormGroup();
    }

    viewRoute() {
        this.currentRoute = Routes.view;
        const id = parseInt(this.route.snapshot.paramMap.get('id'));
        this.contactService.getContact(id)
            .then(contact => {
                this.contact = contact;
                this.contactForm.disable();
                this.isDisabled = true;
                this.fixDeliveryOptionIfNeeded();
                this.createExistingContactFormGroup(contact);
                // dublicate disableing is for possible extra address fileds
                this.contactForm.disable();
            });
    }

    editRoute() {
        this.currentRoute = Routes.edit;
        const id = parseInt(this.route.snapshot.paramMap.get('id'));
        this.contactService.getContact(id)
            .then(contact => {
                this.contact = contact;
                this.fixDeliveryOptionIfNeeded();
                this.createExistingContactFormGroup(contact);
            });
    }

    fixDeliveryOptionIfNeeded() {
        if (this.contact.addresses.length > 1) {
            this.deliveryOption = DeliveryLocation.different.toString();
        }
    }

    chooseRouterLogic() {
        if (this.router.url.includes(Routes.add.toString())) {
            this.addRoute();
        } else if (this.router.url.includes(Routes.view.toString())) {
            this.viewRoute();
        } else if (this.router.url.includes(Routes.edit.toString())) {
            this.editRoute();
        }
    }

    onSubmitAddNewContact() {

        var contact: Contact = this.createContactBasedOnView();

        this.chooseReceiverAddress(contact);

        this.contactService
            .saveContactAndReturn(contact)
            .then((contact) => {
                    this.router.navigateByUrl('/view/' + contact.id);
                }
            );

        this.holdCorrectAddressesCount(contact);

    }

    holdCorrectAddressesCount(contact: Contact) {
        if (this.addresses.length <= 2) {
            for (var i = 0; this.addresses.length < 2; i++) {
                contact.addresses.push(new Address());
                this.addresses.push(this.fb.group(new Address()));
            }
        }
    }

    chooseReceiverAddress(contact: Contact) {
        if (this.deliveryOption == DeliveryLocation.same.toString() && this.currentRoute == Routes.add) {
            contact.addresses.pop();
            this.removeLastAddress();
            contact.addresses[0].receiver = true;
        } else if (this.deliveryOption == DeliveryLocation.different.toString() && this.currentRoute == Routes.add) {
            contact.addresses[1].receiver = true;
        } else if (this.deliveryOption == DeliveryLocation.same.toString() && this.currentRoute == Routes.edit && contact.addresses.length > 1) {
            contact.addresses[0].receiver = true;
            contact.addresses.pop();
            this.removeLastAddress();
        } else if (this.deliveryOption == DeliveryLocation.different.toString() && this.currentRoute == Routes.edit && contact.addresses.length > 1) {
            contact.addresses[0].receiver = false;
            contact.addresses[1].receiver = true;
        }
    }

    onDeliveryOptionChange() {
        this.secondAddressValidationToggler();
    }

    secondAddressValidationToggler() {
        const street = this.contactForm.get('addresses').get('1').get('street');
        const city = this.contactForm.get('addresses').get('1').get('city');
        if (this.deliveryOption === DeliveryLocation.same.toString()) {
            street.setValidators(null);
            city.setValidators(null);
        } else if (this.deliveryOption === DeliveryLocation.different.toString()) {
            street.setValidators([Validators.required, this.noWhitespaceValidator]);
            city.setValidators([Validators.required, this.noWhitespaceValidator]);
        }
        street.updateValueAndValidity();
        city.updateValueAndValidity();
    }

    createContactBasedOnView(): Contact {
        if (this.currentRoute == Routes.add) {
            return this.createContactWithoutId();
        } else if (this.currentRoute == Routes.edit) {
            return this.createContactWithId();
        }
    }

    createContactWithId(): Contact {
        return new Contact(
            this.contactForm.get('fullname').value,
            this.contactForm.get('phone').value,
            this.contactForm.get('mail').value,
            this.contactForm.get('addresses').value,
            this.contactForm.get('awareofterms').value,
            this.contactForm.get('id').value
        )
    }

    createContactWithoutId(): Contact {
        return new Contact(
            this.contactForm.get('fullname').value,
            this.contactForm.get('phone').value,
            this.contactForm.get('mail').value,
            this.contactForm.get('addresses').value,
            this.contactForm.get('awareofterms').value
        )
    }

    createForm() {
        this.contactForm = this.fb.group({
            fullname: '',
            phone: '',
            mail: '',
            addresses: this.fb.array([]),
            awareofterms: ''
        });
    }

    createContactFormGroup() {
        // phone validator regex ref: https://www.regexpal.com/?fam=99127
        const phoneValidatorRegex: string =
            "^(?:(?:\\(?(?:00|\\+)([1-4]\\d\\d|[1-9]\\d?)\\)?)?[\\-\\.\\ \\\\\\/]?)?((?:\\(?\\d{1,}\\)?[\\-\\.\\ \\\\\\/]?){0,})(?:[\\-\\.\\ \\\\\\/]?(?:#|ext\\.?|extension|x)[\\-\\.\\ \\\\\\/]?(\\d+))?$";

        this.contactForm = new FormGroup({
                'fullname': new FormControl(null, [Validators.required, this.noWhitespaceValidator]),
                'phone': new FormControl(null, [Validators.pattern(phoneValidatorRegex), this.noWhitespaceValidator]),
                'mail': new FormControl(null, Validators.email),
                'addresses': new FormArray([
                    new FormGroup({
                        'street': new FormControl(null, [Validators.required, this.noWhitespaceValidator]),
                        'village': new FormControl(null),
                        'city': new FormControl(null, [Validators.required, this.noWhitespaceValidator]),
                        'receiver': new FormControl(false)
                    })
                ]),
                'awareofterms': new FormControl(null, Validators.requiredTrue),
            }, this.oneMustHaveValueValidator
        );
    }

    noWhitespaceValidator(control: AbstractControl): ValidationErrors {
        let isWhitespace = (control.value || '').trim().length === 0;
        let isValid = !isWhitespace;
        return isValid ? null : {'whitespace': 'value is only whitespace'}
    }

    oneMustHaveValueValidator(g: FormGroup) {
        let phone = g.controls['phone'];
        let mail = g.get("mail");

        if (phone.value !== null || mail.value !== null) {
            return null;
        }

        return {'oneOfTwoHasValue': true}
    }

    createExistingContactFormGroup(contact: Contact) {

        var formArray = this.createFormArrayForExistingContactFormGroup(contact);

        const phoneValidatorRegex: string =
            "^(?:(?:\\(?(?:00|\\+)([1-4]\\d\\d|[1-9]\\d?)\\)?)?[\\-\\.\\ \\\\\\/]?)?((?:\\(?\\d{1,}\\)?[\\-\\.\\ \\\\\\/]?){0,})(?:[\\-\\.\\ \\\\\\/]?(?:#|ext\\.?|extension|x)[\\-\\.\\ \\\\\\/]?(\\d+))?$";

        this.contactForm = new FormGroup({
                'id': new FormControl(contact.id),
                'fullname': new FormControl(contact.fullName, [Validators.required, this.noWhitespaceValidator]),
                'phone': new FormControl(contact.phone, [Validators.pattern(phoneValidatorRegex), this.noWhitespaceValidator]),
                'mail': new FormControl(contact.mail, Validators.email),
                'addresses': formArray,
                'awareofterms': new FormControl(contact.awareOfTerms, Validators.requiredTrue),
            }, this.oneMustHaveValueValidator
        );
    }

    createFormArrayForExistingContactFormGroup(contact: Contact): FormArray {
        let formArray = new FormArray([
            new FormGroup({
                'street': new FormControl(contact.addresses[0].street),
                'village': new FormControl(contact.addresses[0].village),
                'city': new FormControl(contact.addresses[0].city),
                'receiver': new FormControl(contact.addresses[0].receiver)
            })
        ]);

        if (contact.addresses.length == 2) {
            formArray.push(new FormGroup({
                    'street': new FormControl(contact.addresses[1].street),
                    'village': new FormControl(contact.addresses[1].village),
                    'city': new FormControl(contact.addresses[1].city),
                    'receiver': new FormControl(contact.addresses[1].receiver)
                })
            );
        } else {
            //add extra address in case contact chooses to have 2 addresses
            formArray.push(new FormGroup({
                    'street': new FormControl(null),
                    'village': new FormControl(null),
                    'city': new FormControl(null),
                    'receiver': new FormControl(null)
                })
            );
        }
        return formArray;
    }

    get addresses(): FormArray {
        return this.contactForm.get('addresses') as FormArray;
    };

    addAddress() {
        this.addresses.push(this.fb.group(new Address()));
    }

    removeLastAddress() {
        this.addresses.removeAt(this.addresses.length - 1);
    }

}
