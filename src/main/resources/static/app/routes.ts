import {Routes} from '@angular/router';

import {ListComponent} from './list/list.cmp';
import {ViewComponent} from './form/form.cmp';
import {HomeComponent} from "./home/home.cmp";

export const routes: Routes = [
    {path: 'add', component: ViewComponent, data: {title: 'Add Contact'}},
    {path: 'list', component: ListComponent, data: {title: 'Contacts List'}},
    {path: 'home', component: HomeComponent, data: {title: 'Icefire Contacts Application'}},
    {path: 'view/:id', component: ViewComponent, data: {title: 'View Contact'}},
    {path: 'edit/:id', component: ViewComponent, data: {title: 'Edit Contact'}},
    {path: '', redirectTo: 'home', pathMatch: 'full'}
];