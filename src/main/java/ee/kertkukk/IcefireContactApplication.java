package ee.kertkukk;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IcefireContactApplication {

	public static void main(String[] args) {
		SpringApplication.run(IcefireContactApplication.class, args);
	}
}
