package ee.kertkukk.dao;

import ee.kertkukk.model.Contact;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Kert on 02/11/2017.
 */
public interface ContactDao extends CrudRepository<Contact, Long> {



}
