package ee.kertkukk.dao;

import ee.kertkukk.model.Address;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Kert on 02/11/2017.
 */
public interface AddressDao extends CrudRepository<Address, Long> {
}
