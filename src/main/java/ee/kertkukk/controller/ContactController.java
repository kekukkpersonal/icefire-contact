package ee.kertkukk.controller;

import ee.kertkukk.dao.ContactDao;
import ee.kertkukk.model.Address;
import ee.kertkukk.model.Contact;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Kert on 02/11/2017.
 */
@RestController
public class ContactController {

    @Resource
    private ContactDao dao;

    @GetMapping("api/contacts")
    public Iterable<Contact> contacts() {
        return dao.findAll();
    }

    @PostMapping("api/contacts")
    public void saveContact(@RequestBody @Valid Contact contact) {
        dao.save(contact);
    }

    @PostMapping("api/contact")
    public Contact saveContactAndReturn(@RequestBody @Valid Contact contact) {
        dao.save(contact);
        return contact;
    }

    @GetMapping("api/contact/{id}")
    private Contact editContact(@PathVariable("id") Long id) {
        return dao.findOne(id);
    }

    @DeleteMapping("api/contact/{id}")
    private void deleteContact(@PathVariable("id") Long id) {
        dao.delete(id);
    }

}
