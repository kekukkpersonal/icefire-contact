package ee.kertkukk.controller;

import ee.kertkukk.dao.AddressDao;
import ee.kertkukk.model.Address;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * Created by Kert on 02/11/2017.
 */

@RestController
public class AddressController {

    @Resource
    private AddressDao dao;

    @GetMapping("api/addresses")
    private Iterable<Address> addresses() {
        return dao.findAll();
    }

    @PostMapping("api/addresses")
    private void saveAddress(@RequestBody Address address) {
        dao.save(address);
    }

    @GetMapping("api/address/{id}")
    private Address editAddress(@PathVariable("id") Long id) { return dao.findOne(id); }

//    @DeleteMapping("api/address/{id}")
//    private void deleteAddress(@PathVariable("id") Long id) { dao.delete(id); }

}
