package ee.kertkukk.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Created by Kert on 15/11/2017.
 */
public class NullOrNotBlankValidator implements ConstraintValidator<NullOrNotBlank, String> {

   public void initialize(NullOrNotBlank parameters) {
      // Nothing to do here
   }

   public boolean isValid(String value, ConstraintValidatorContext constraintValidatorContext) {
      if (value == null) {
         return true;
      }
      if (value.length() == 0) {
         return false;
      }

      boolean isAllWhitespace = value.matches("^\\s*$");
      return !isAllWhitespace;
   }
}
