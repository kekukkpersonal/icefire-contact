package ee.kertkukk.validation;

import org.apache.commons.beanutils.BeanUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Created by Kert on 02/11/2017.
 */

public class HasValueValidator implements ConstraintValidator<HasValue, Object> {
    private String firstFieldName;
    private String secondFieldName;

    @Override
    public void initialize(final HasValue constraintAnnotation) {
        firstFieldName = constraintAnnotation.first();
        secondFieldName = constraintAnnotation.second();
    }

    @Override
    public boolean isValid(final Object value, final ConstraintValidatorContext context) {
        try {
            final Object firstObj = BeanUtils.getProperty(value, firstFieldName);
            final Object secondObj = BeanUtils.getProperty(value, secondFieldName);

            return firstObj != null || secondObj != null;
        } catch (final Exception ignore) {
            // ignore
        }
        return true;
    }
}