package ee.kertkukk.model;

import ee.kertkukk.validation.HasValue;
import ee.kertkukk.validation.NullOrNotBlank;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.List;

/**
 * Created by Kert on 02/11/2017.
 */

@Data
@Entity
@HasValue(first = "phone", second = "mail", message = "phone or mail should not be null")
public class Contact {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    //    https://stackoverflow.com/questions/275160/regex-for-names
    @NotBlank
    @Pattern(regexp = "^[A-Za-z]+((\\s)?((\\'|\\-|\\.)?([A-Za-z])+))*$")
    private String fullName;
    //    https://www.regexpal.com/?fam=99127
    @NullOrNotBlank
    @Pattern(regexp = "^(?:(?:\\(?(?:00|\\+)([1-4]\\d\\d|[1-9]\\d?)\\)?)?[\\-\\.\\ \\\\\\/]?)?((?:\\(?\\d{1,}\\)?[\\-\\.\\ \\\\\\/]?){0,})(?:[\\-\\.\\ \\\\\\/]?(?:#|ext\\.?|extension|x)[\\-\\.\\ \\\\\\/]?(\\d+))?$")
    private String phone;
    @NullOrNotBlank
    @Email
    private String mail;
    @NotNull
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "address_id")
    private List<Address> addresses;
    @NotNull
    private Boolean awareOfTerms;

}
