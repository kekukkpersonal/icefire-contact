package ee.kertkukk.model;

import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by Kert on 02/11/2017.
 */
@Data
@Entity
public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "address_id", insertable = false, updatable = false)
    private Long customer_id;

    @NotBlank
    private String street;
    private String village;
    @NotBlank
    private String city;
    @NotNull
    private Boolean receiver = false;

}
